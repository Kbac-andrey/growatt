import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-radiator-modal',
  templateUrl: './radiator-modal.component.html',
  styleUrls: ['./radiator-modal.component.scss'],
})
export class RadiatorModalComponent implements OnInit {
  public form!: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<RadiatorModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      action$: (
        config: any,
      ) => Observable<any>;
    },
    private _formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
   this.form = this._formBuilder.group({
     capacity: '',
     resources: ''
   });
  }

  public close(): void {
    this.dialogRef.close();
  }

  public submit(): void {
    this.close();

    this.data
      .action$({applied: true, formValue: this.form.value})
  }

}
