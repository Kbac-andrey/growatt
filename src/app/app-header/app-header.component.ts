import { Component, ViewEncapsulation } from '@angular/core';
import { SideNavService } from '../services/side-nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppHeaderComponent {
  public toggle: boolean = false;
  constructor(private _sideNavService: SideNavService) { }

  public toggleSideNav(value: boolean): void {
    this._sideNavService.setShowNav(value);
  }

}
