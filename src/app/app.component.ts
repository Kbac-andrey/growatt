import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { GebouwModalComponent } from './gebouw-modal/gebouw-modal.component';
import { MetterModalComponent } from './metter-modal/metter-modal.component';
import { PannelModalComponent } from './pannel-modal/pannel-modal.component';
import { CarModalComponent } from './car-modal/car-modal.component';
import { StationModalComponent } from './station-modal/station-modal.component';
import { RadiatorModalComponent } from './radiator-modal/radiator-modal.component';
import { SideNavService } from './services/side-nav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
 public title: string = 'growatt';
 public gebouw: string = '';
 public verbruik: string = '';
 public dynamicChargingAlreadyActive: string = '';
 public zonnepanelen: boolean | undefined = undefined;
 public carActiveDynamicCharging: boolean | undefined = undefined;
 public isRadiatorApply: boolean = false;
 public isShowSidePanel: boolean = false;
 public formValues: any[] = [
   {title: 'boerderij', status: "Not applied", value: null},
   {title: 'Verbruik', status: "Not applied", value: null},
   {title: 'Zonnepanelen', status: "Not applied", value: null},
   {title: 'Laadpunt(en)', status: "Not applied", value: null},
   {title: 'Aansluiting', status: "Not applied", value: null},
   {title: 'Batterij', status: "Not applied", value: null},
 ];


  public constructor(private _dialog: MatDialog, private _sideNavService: SideNavService) {}

  public ngOnInit(): void {
    this._sideNavService.getShowNav().subscribe((isShowSidePanel: boolean) => {
      console.log(this.isShowSidePanel);
      this.isShowSidePanel = isShowSidePanel
    });
  }

  public openGebouwModal(typeOfGebouw: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = "gebouw-modal-component";
    dialogConfig.height = "320px";
    dialogConfig.width = "550px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      typeOfGebouw: typeOfGebouw,
      title: 'Gebouw',
      action$: (gebouwValue: string) => {
        this.gebouw = gebouwValue;
        this.formValues[0] = { title: 'boerderij', status: "applied", value: { Gebouw: gebouwValue} };
      }
    }
    this._dialog.open(GebouwModalComponent, dialogConfig);
  }
  public openMetterModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = "metter-modal-component";
    dialogConfig.height = "311px";
    dialogConfig.width = "296px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      title: 'Verbruik',
      action$: (verbruikFormValue:any) => {
        this.verbruik = verbruikFormValue.verbruik;
        this.formValues[1] = { title: 'Verbruik', status: "applied", value:  verbruikFormValue };
      }


    }
     this._dialog.open(MetterModalComponent, dialogConfig);
  }

  public openPanelModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = "pannel-modal-component";
    dialogConfig.height = "229px";
    dialogConfig.width = "296px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      title: 'Zonnepanelen:',
      action$: (zonnepanelen: any) => {
        this.zonnepanelen = zonnepanelen.productionData
        this.formValues[2] = { title: 'Zonnepanelen', status: "applied", value: zonnepanelen};
      }

    }
    this._dialog.open(PannelModalComponent, dialogConfig);
  }

  public openCarModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = "car-modal-component";
    dialogConfig.height = "368px";
    dialogConfig.width = "296px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      title: 'Laadpunt(en):',
      action$: (isActiveDynamicCharging: any) => {
        this.carActiveDynamicCharging = isActiveDynamicCharging.activeDynamicCharging,
        this.formValues[3] = { title: 'Laadpunt(en)', status: "applied", value: isActiveDynamicCharging};
      }
    }
    this._dialog.open(CarModalComponent, dialogConfig);
  }

  public openStationModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = "station-modal-component";
    dialogConfig.height = "289px";
    dialogConfig.width = "320px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      title: 'Aansluiting:',
      action$: (dynamicChargingAlreadyActive: any) => {
        this.dynamicChargingAlreadyActive = dynamicChargingAlreadyActive.dynamicChargingAlreadyActive
        this.formValues[4] = { title: 'Aansluiting', status: "applied", value: dynamicChargingAlreadyActive};
      }

    }
    this._dialog.open(StationModalComponent, dialogConfig);
  }

  public openRadiatorModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.id = "radiator-modal-component";
    dialogConfig.height = "330px";
    dialogConfig.width = "296px";
    dialogConfig.autoFocus = false;
    dialogConfig.data = {
      title: 'Batterij:',
      action$: (isRadiatorApply: any) => {
        this.isRadiatorApply = isRadiatorApply.applied
        this.formValues[5] = { title: 'Batterij', status: "applied", value: isRadiatorApply.formValue};
      }
    }
    this._dialog.open(RadiatorModalComponent, dialogConfig);
  }
}
