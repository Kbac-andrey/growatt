import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { GebouwModalComponent } from './gebouw-modal/gebouw-modal.component';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button'
import { MatSidenavModule } from '@angular/material/sidenav';
import { ReactiveFormsModule } from '@angular/forms';
import { MetterModalComponent } from './metter-modal/metter-modal.component';
import { PannelModalComponent } from './pannel-modal/pannel-modal.component';
import { CarModalComponent } from './car-modal/car-modal.component';
import { StationModalComponent } from './station-modal/station-modal.component';
import { RadiatorModalComponent } from './radiator-modal/radiator-modal.component';
import { AppHeaderComponent } from './app-header/app-header.component';



@NgModule({
  declarations: [
    AppComponent,
    GebouwModalComponent,
    MetterModalComponent,
    PannelModalComponent,
    CarModalComponent,
    StationModalComponent,
    RadiatorModalComponent,
    AppHeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
