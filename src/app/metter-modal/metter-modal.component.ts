import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-metter-modal',
  templateUrl: './metter-modal.component.html',
  styleUrls: ['./metter-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MetterModalComponent implements OnInit {
  public form!: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<MetterModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      action$: (
        config: any,
      ) => Observable<any>;
    },
    private _formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
   this.form = this._formBuilder.group({
     verbruik: 'verbruikdata'
   });
  }

  public close(): void {
    this.dialogRef.close();
  }

  public submit(): void {
    this.close();

    this.data
      .action$(this.form.value)
  }

}
