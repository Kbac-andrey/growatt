import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-station-modal',
  templateUrl: './station-modal.component.html',
  styleUrls: ['./station-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StationModalComponent implements OnInit {
  public form!: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<StationModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      action$: (
        config: any,
      ) => Observable<any>;
    },
    private _formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
   this.form = this._formBuilder.group({
     dynamicChargingAlreadyActive: 'kleinverbruik',
     dynamicChargingType: ''
   });
  }

  public close(): void {
    this.dialogRef.close();
  }

  public submit(): void {
    this.close();

    this.data
      .action$(this.form.value)
  }

}
