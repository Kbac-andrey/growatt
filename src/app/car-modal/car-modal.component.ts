import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-car-modal',
  templateUrl: './car-modal.component.html',
  styleUrls: ['./car-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarModalComponent implements OnInit {
  public form!: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<CarModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      title: string;
      action$: (
        config: any,
      ) => Observable<any>;
    },
    private _formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
   this.form = this._formBuilder.group({
     numberOfChargingPoints: '',
     activeDynamicCharging: true,
     insuredCapital: ''
   });
  }

  public close(): void {
    this.dialogRef.close();
  }

  public submit(): void {
    this.close();

    this.data
      .action$(this.form.value)
  }

}
