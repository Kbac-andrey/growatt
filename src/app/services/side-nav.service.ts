import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideNavService {
  private showNav$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  setShowNav(showHide: boolean) {
    this.showNav$.next(showHide);
  }

  getShowNav(){
    return this.showNav$.asObservable();
  }
}
