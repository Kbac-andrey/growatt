import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gebouw-modal',
  templateUrl: './gebouw-modal.component.html',
  styleUrls: ['./gebouw-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GebouwModalComponent implements OnInit {
  public form!: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<GebouwModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      typeOfGebouw: string
      title: string;
      action$: (
        config: any,
      ) => Observable<any>;
    },
    private _formBuilder: FormBuilder,
  ) {}

  public ngOnInit(): void {
   this.form = this._formBuilder.group({
     gebouw: this.data.typeOfGebouw || 'boerderij',
   });
  }

  public close(): void {
    this.dialogRef.close();
  }

  public submit(): void {
    this.close();

    this.data
      .action$(this.form.controls['gebouw'].value)
  }

}
